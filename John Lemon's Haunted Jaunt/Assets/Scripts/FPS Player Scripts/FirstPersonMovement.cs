using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMovement : MonoBehaviour
{
    public float speed = 1f;
    public GameObject weapon;
    public GameObject GateController;

    private CharacterController controller;
    private Animator m_Animator;
    private Vector3 m_Movement;
    private AudioSource m_AudioSource;
    private bool reloading;
    private GameObject currentCollider;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        m_Animator = GetComponentInChildren<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        m_Movement = transform.right * horizontal + transform.forward * vertical;

        controller.Move(m_Movement * speed * Time.deltaTime);

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        bool isShooting = Input.GetMouseButton(0); // will need to change this
        m_Animator.SetBool("IsWalking", isWalking);
        m_Animator.SetBool("IsShooting", weapon.GetComponent<ShootLaser>().getShooting());
        m_Animator.SetBool("IsReloading", reloading);

        if (Input.GetKey(KeyCode.R) && !Input.GetMouseButton(0) && weapon.GetComponent<ShootLaser>().getCharge() < 100)
        {
            // player has hit the r key while not shooting, and with a current charge less that 100
            reloading = true;
            StartCoroutine(stopReloading());
            StartCoroutine(weapon.GetComponent<ShootLaser>().chargeGun());
        }

        if (isWalking)
        {
            // if the audiosource is not already playing, play the AudioSource
            if (!m_AudioSource.isPlaying) m_AudioSource.Play();
        }
        else
        {
            m_AudioSource.Stop();
        }
    }

    IEnumerator stopReloading()
    {
        yield return new WaitForSeconds(2.91f);
        reloading = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "CloseGateOne")
        {
            GateController.GetComponent<GateScript>().closeGateOne();
            currentCollider = other.gameObject;
            StartCoroutine(playVoiceLineTwo());
        }
        if (other.gameObject.name == "CloseGateTwo")
        {
            GateController.GetComponent<GateScript>().closeGateTwo();
            currentCollider = other.gameObject;
            StartCoroutine(playVoiceLineThree());
        }
        if (other.gameObject.name == "CloseGateThree")
        {
            GateController.GetComponent<GateScript>().closeGateThree();
        }
        // gate four is on a timer
        if (other.gameObject.name == "CloseGateFive")
        {
            GateController.GetComponent<GateScript>().closeGateFive();
        }
        // gate six is on a timer
        // gate seven is on a timer
    }

    IEnumerator playVoiceLineTwo()
    {
        yield return new WaitForSeconds(2);
        currentCollider.GetComponent<AudioSource>().Play();
    }

    IEnumerator playVoiceLineThree()
    {
        yield return new WaitForSeconds(2);
        currentCollider.GetComponent<AudioSource>().Play();
    }
}
