using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam
{
    private Vector3 pos, dir;
    public GameObject laserObj;
    private LineRenderer laser;
    private List<Vector3> laserIndices = new List<Vector3>();
    private Vector3 RayCastEnd;
    private Vector3 hitObjectSize;
    private static float laserDistance = 10f;
    private static int laserDamage = 50;
    private bool throughObject;

    // button booleans
    private static bool buttonOne = false; // false when the button is not hit
    private static bool buttonTwo = false;
    private bool buttonThree = false; // shouldn't be static, for the timer buttons (I think)
    private  bool buttonFour = false;
    private static bool buttonFive = false; // timed
    private bool buttonSix = false; // timed

    public LaserBeam(Vector3 pos, Vector3 dir, ParticleSystem laserHit, Material material)
    {
        throughObject = false;

        this.laserObj = new GameObject();
        this.laserObj.name = "Laser Beam";
        this.pos = pos;
        this.dir = dir;

        this.laser = this.laserObj.AddComponent(typeof(LineRenderer)) as LineRenderer;
        this.laser.startWidth = 0.2f;
        this.laser.endWidth = 0.2f;
        this.laser.material = material;     
        CastRay(pos, dir, laser);
    }

    void CastRay(Vector3 pos, Vector3 dir, LineRenderer laser)
    {
        laserIndices.Add(pos);
        UpdateLaser();

        Ray ray = new Ray(pos, dir);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, laserDistance, 1))
        {
            // if the laser gets a hit, it is added to the laserIndices ArrayList
            CheckHit(hit, dir, laser);
        }
        else
        {
            RayCastEnd = pos + dir * laserDistance; // needs to happen before setting the position
            if (laser.positionCount == 0)
            {
                // LaserIndeces has no previous indices, so we need to create 2 so that a line renderer shows up
                laser.positionCount = 2;
                laser.SetPosition(0, pos); // first position
                laser.SetPosition(1, RayCastEnd); // last position
            }
            else
            { 
                // LaserIndices has previous indices, so the last linerenderer needs to be created
                laser.positionCount += 2; // add two more indices to the linerenderer
                laser.SetPosition(laser.positionCount - 2, pos); // second to last position
                laser.SetPosition(laser.positionCount - 1, RayCastEnd); // last position
            }
            
        }
    }

    void UpdateLaser()
    {
        int count = 0;
        laser.positionCount = laserIndices.Count;

        foreach (Vector3 idx in laserIndices)
        {
            laser.SetPosition(count, idx);
            count++;
        }
    }

    void CheckHit(RaycastHit hitInfo, Vector3 direction, LineRenderer laser)
    {
        // start with checking if the player hit a button
        if (hitInfo.collider.name == "ButtonOne")
        {
            buttonOne = true;
        }

        if (hitInfo.collider.name == "ButtonTwo")
        {
            buttonTwo = true;
        }

        if (hitInfo.collider.name == "ButtonThree")
        {
            buttonThree = true;
        }

        if (hitInfo.collider.name == "ButtonFour")
        {
            buttonFour = true;
        }

        if (hitInfo.collider.name == "ButtonFive")
        {
            buttonFive = true;
        }

        if (hitInfo.collider.name == "ButtonSix")
        {
            buttonSix = true;
        }

        if (hitInfo.collider.gameObject.transform.root.tag == "Mirror" || hitInfo.collider.tag == "Mirror") // checking if the overall parent object has the tag Mirror
        {
            Vector3 pos = hitInfo.point;
            Vector3 dir = Vector3.Reflect(direction, hitInfo.normal);

            CastRay(pos, dir, laser);
        }
        else if (hitInfo.collider.gameObject.transform.root.tag != "Player")
        {
            // create the ray, but don't reflect it.
            Vector3 pos = hitInfo.point;
            laserIndices.Add(pos); // add to the position list
            laser.positionCount += 1;
            laser.SetPosition(laser.positionCount - 1, pos); // set last position to pos of raycast hit

        }

        if(hitInfo.collider.tag == "Enemy")
        {
            EnemyBase enemy = hitInfo.collider.GetComponent<EnemyBase>();
            enemy.subtractEnemyHealth(laserDamage);
        }

        if(hitInfo.collider.tag == "Obstacle")
        {
            hitInfo.collider.GetComponent<DissolveTest>().DrawLaser(hitInfo, hitInfo.collider.gameObject);
            //hitInfo.collider.GetComponent<Renderer>().sharedMaterial.SetVector("Point", hitInfo.point);
            hitObjectSize = hitInfo.collider.bounds.size;
            throughObject = true;
            dir = direction;
            pos = hitInfo.point;
        }
    }

    public List<Vector3> getLaserIndices()
    {
        return laserIndices;
    } 

    public void setRenderQueue(int a)
    {
        this.laser.material.renderQueue = a;
    }

    public int getRenderQueue()
    {
        return this.laser.material.renderQueue;
    }

    public Vector3 getHitObjectSize()
    {
        if (hitObjectSize != null)
        {
            return hitObjectSize;
        }
        else
        {
            return new Vector3(0f, 0f, 0f);
        }
    }

    public Vector3 getDirection()
    {
        return dir;
    }

    public Vector3 getPosition()
    {
        return pos;
    }

    public bool getThroughObject()
    {
        return throughObject;
    }

    public bool getButtonOne()
    {
        return buttonOne;
    }

    public bool getButtonTwo()
    {
        return buttonTwo;
    }

    public bool getButtonThree()
    {
        return buttonThree;
    }

    public bool getButtonFour()
    {
        return buttonFour;
    }

    public bool getButtonFive()
    {
        return buttonFive;
    }

    public bool getButtonSix()
    {
        return buttonSix;
    }
}
