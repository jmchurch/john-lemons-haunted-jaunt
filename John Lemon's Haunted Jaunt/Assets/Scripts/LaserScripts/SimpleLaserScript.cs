using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLaserScript : MonoBehaviour
{
    public GameObject laserPrefab;
    public GameObject muzzle;

    private GameObject spawnedLaser;

    // Start is called before the first frame update
    void Start()
    {
        spawnedLaser = Instantiate(laserPrefab, muzzle.transform) as GameObject;
        disableLaser();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            enableLaser();
        }

        if (Input.GetMouseButton(0))
        {
            updateLaser();
        }

        if (Input.GetMouseButtonUp(0))
        {
            disableLaser();
        }
    }

    void enableLaser()
    {
        spawnedLaser.SetActive(true);
    }

    void disableLaser()
    {
        spawnedLaser.SetActive(false);
    }

    void updateLaser()
    {
        if (muzzle.transform != null)
        {
            spawnedLaser.transform.position = muzzle.transform.position;
        }
    }
}
