using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ShootLaser : MonoBehaviour
{
    public GameObject gateController;
    public GameObject laserPrefab;
    public GameObject muzzle;
    public Material laserMaterial;
    public ParticleSystem beamStart;
    public ParticleSystem sparksSystem;
    public GameObject beamHit;
    public AudioSource LaserShootAudio;
    public AudioSource LaserChargeAudio;

    public LaserChargeMeter chargeMeter;
    LaserBeam beam; // beam that reflects off of object
    LaserBeam newBeam;

    private ParticleSystem beamOrigin;
    private ParticleSystem beamSparks;
    private ParticleSystem.EmissionModule sparkEmmision;
    private int laserCharge = 100;
    private bool removingCharge = false;
    private bool shooting = false;
    private bool chargingSoundEffect = false;

    // booleans that track whether the gate has been opened (only applicable if they are opened by buttons)
    private bool gateTwo = false;
    private bool gateThree = false;
    private bool gateFour = false;
    private bool gateFive = false;
    private bool gateSix = false;
    private bool gateSeven = false;

    private static bool shootingAvailable = false;

    private void Start()
    {
        chargeMeter.setMaxCharge(laserCharge);
        beamOrigin = Instantiate(beamStart, muzzle.transform);
        beamSparks = Instantiate(sparksSystem, muzzle.transform);
        sparkEmmision = beamSparks.emission; // allow for access to the emmision modifier
        beamOrigin.Stop();
        sparkEmmision.enabled = false; // remove emmission in start

        if (beam != null)
        {
            Destroy(beam.laserObj);
        }
        if (newBeam != null){
            Destroy(newBeam.laserObj);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (shooting)
        {
            sparkEmmision.enabled = true; // allow emmission when shooting
            beamOrigin.Play();
        }
        if (Input.GetMouseButton(0) && laserCharge != 0 && shootingAvailable)
        {
            if (!chargingSoundEffect)
            {
                if (!LaserChargeAudio.isPlaying)
                {
                    LaserChargeAudio.Play(); // play charging effect only if the charging effect isn't already being played
                    StartCoroutine(playChargeAudio()); // call coroutine that waits to start up laser
                }
            }
            if (chargingSoundEffect) // once the charge effect is played the laser will be enabled
            {
                enableLaser();
            }
        } 
        else if (!Input.GetMouseButton(0) || laserCharge <= 0) // mouse button1 is not being held down
        {
            // both beamOrgin and sparkEmmission should wind down while the tiling is being changed
            beamOrigin.Stop();
            sparkEmmision.enabled = false; // disable emmission when not shooting
            disableLaser();
        }
    }

    IEnumerator playChargeAudio()
    {
        yield return new WaitForSeconds(5f);
        chargingSoundEffect = true; // allows for enableLaser() to start
    }

    public IEnumerator chargeGun()
    {
        yield return new WaitForSeconds(2.91f);
        laserCharge = 100;
        chargeMeter.setCharge(laserCharge);
        chargingSoundEffect = false;
        
    }

    IEnumerator removeCharge()
    {
        removingCharge = true; // needs to be called before WaitForSeconds
                               // Reason: Multiple recursive calls for removeCharge will be created (look to line 1 in enableLaser method)
        yield return new WaitForSeconds(.06f);
        laserCharge--;
        chargeMeter.setCharge(laserCharge);
        if(Input.GetMouseButton(0) && laserCharge > 0)
        {
            StartCoroutine(removeCharge());
        }
        else
        {
            removingCharge = false;
        }
    }

    void enableLaser()
    {
        if (!LaserShootAudio.isPlaying)
        {
            LaserShootAudio.Play(); // play the laser sound only if it isn't already being played
        }
        shooting = true; // laser is shooting
        if (!removingCharge) StartCoroutine(removeCharge());
        //set the length of the original laser, can make this more efficient once waves are added
        if (beam != null)
        {
            Destroy(beam.laserObj);
        }
        if (newBeam != null)
        {
            Destroy(newBeam.laserObj);
        }
        beam = new LaserBeam(muzzle.transform.position, muzzle.transform.forward, laserPrefab.GetComponentInChildren<ParticleSystem>(), laserMaterial);
        // check button statuses
        if (beam.getButtonOne() && !gateTwo)
        {
            StartCoroutine(gateController.GetComponent<GateScript>().openGateTwo());
            gateTwo = true; // the gate has been opened
        }
        if (beam.getButtonTwo() && !gateThree)
        {
            StartCoroutine(gateController.GetComponent<GateScript>().openGateThree());
            gateThree = true; // gate three was opened
        }
        if (beam.getButtonThree() && !gateFour)
        {
            StartCoroutine(gateController.GetComponent<GateScript>().openGateFour());
            StartCoroutine(gateController.GetComponent<GateScript>().closeGateFour());
            gateFour = true; // gate four was opened, this will be set back to false in 3.5 sec after the IEnumerator called finishes
        }
        // for gateFive, gateSix, and gateSeven it needs to have this same if statement in the newBeam area, since the mechanic it uses makes a new beam instance
        if (beam.getButtonFour() && !gateFive)
        {
            StartCoroutine(gateController.GetComponent<GateScript>().openGateFive());
            gateFive = true; // gate five was opened
        }
        if (beam.getButtonFive() && !gateSix)
        {
            StartCoroutine(gateController.GetComponent<GateScript>().openGateSix());
            StartCoroutine(gateController.GetComponent<GateScript>().closeGateSix());
            gateSix = true; // gate six was opened
        }
        if (beam.getButtonSix() && !gateSeven)
        {
            StartCoroutine(gateController.GetComponent<GateScript>().openGateSeven());
            StartCoroutine(gateController.GetComponent<GateScript>().closeGateSeven());
            gateSeven = true; // gate seven was opened
        }
        if (beam.getThroughObject())
        {
            Vector3 beamDirection = beam.getDirection();
            if (Mathf.Abs(beamDirection.z) > Mathf.Abs(beamDirection.x))
            {
                if (beamDirection.z > 0){
                    newBeam = new LaserBeam(beam.getPosition() + new Vector3(0f, 0f, beam.getHitObjectSize().z), beamDirection, laserPrefab.GetComponent<ParticleSystem>(), laserMaterial);
                }
                else
                { 
                    newBeam = new LaserBeam(beam.getPosition() - new Vector3(0f, 0f, beam.getHitObjectSize().z), beamDirection, laserPrefab.GetComponent<ParticleSystem>(), laserMaterial);
                }
            }
            else 
            {
                if (beamDirection.x > 0)
                {
                    newBeam = new LaserBeam(beam.getPosition() + new Vector3(beam.getHitObjectSize().x, 0f, 0f), beamDirection, laserPrefab.GetComponent<ParticleSystem>(), laserMaterial);
                }
                else
                {
                    newBeam = new LaserBeam(beam.getPosition() - new Vector3(beam.getHitObjectSize().x, 0f, 0f), beamDirection, laserPrefab.GetComponent<ParticleSystem>(), laserMaterial);
                }
            }
            if (newBeam.getButtonFour() && !gateFive)
            {
                StartCoroutine(gateController.GetComponent<GateScript>().openGateFive());
                gateFive = true; // gate five was opened
            }
            if (newBeam.getButtonFive() && !gateSix)
            {
                StartCoroutine(gateController.GetComponent<GateScript>().openGateSix());
                StartCoroutine(gateController.GetComponent<GateScript>().closeGateSix());
                gateSix = true; // gate six was opened
            }
            if (newBeam.getButtonSix() && !gateSeven)
            {
                StartCoroutine(gateController.GetComponent<GateScript>().openGateSeven());
                StartCoroutine(gateController.GetComponent<GateScript>().closeGateSeven());
                gateSeven = true; // gate seven was opened
            }
            newBeam.setRenderQueue(beam.getRenderQueue() - 2);
        }
    }

    void disableLaser()
    {
        LaserShootAudio.Pause(); // pause the laser shoot sound
        shooting = false;
        removingCharge = false;
        if (beam != null)
        {
            Destroy(beam.laserObj);
        }
        if (newBeam != null)
        {
            Destroy(newBeam.laserObj);
        }
    }

    public bool getShooting()
    {
        return shooting;
    }

    public int getCharge()
    {
        return laserCharge;
    }

    public void setShootingAvailable(bool a)
    {
        shootingAvailable = a;
    }

    public void setGateFour(bool a)
    {
        gateFour = a;
    }

    public void setGateSix(bool a)
    {
        gateSix = a;
    }

    public void setGateSeven(bool a)
    {
        gateSeven = a;
    }
}
