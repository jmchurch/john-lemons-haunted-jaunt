using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    private int enemyHealth;

    private void Start()
    {
        enemyHealth = 100;
    }

    private void Update()
    {
        if (enemyHealth < 0) Destroy(this.gameObject);
    }

    public void subtractEnemyHealth(int healthDecrement)
    {
        enemyHealth -= healthDecrement;
    }
}
