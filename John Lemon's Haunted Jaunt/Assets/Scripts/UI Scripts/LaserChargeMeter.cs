using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserChargeMeter : MonoBehaviour
{

    public Slider slider;

    public void setCharge(int charge)
    {
        slider.value = charge;
    }

    public void setMaxCharge(int maxCharge)
    {
        slider.maxValue = maxCharge;
        slider.value = maxCharge;
    }
}
