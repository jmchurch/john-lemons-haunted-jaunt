using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitSystem : MonoBehaviour
{
    public Collider quitCollider;
    public Light quitLight;
    public GameObject Three;
    public GameObject Two;
    public GameObject One;


    private bool inCollider = false;
    private bool threeDone = false;
    private bool twoDone = false;
    private bool oneDone = false;

    private void Update()
    {
        if (inCollider && !threeDone)
        {
            StartCoroutine(enableThree());
        }

        if (threeDone && !twoDone)
        {
            StartCoroutine(enableTwo());
        }

        if (twoDone && !oneDone)
        {
            StartCoroutine(enableOne());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            quitLight.enabled = true;
            inCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            quitLight.enabled = false;
            threeDone = false; // reset bools to default
            twoDone = false;
            oneDone = false;
            inCollider = false;

            Three.SetActive(false);
            Two.SetActive(false);
            One.SetActive(false);
        }
    }

    IEnumerator enableThree()
    {
        Three.SetActive(true);
        yield return new WaitForSeconds(1f);
        Three.SetActive(false);
        threeDone = true;
    }

    IEnumerator enableTwo()
    {
        Two.SetActive(true);
        yield return new WaitForSeconds(1f);
        Two.SetActive(false);
        twoDone = true;
    }

    IEnumerator enableOne()
    {
        One.SetActive(true);
        yield return new WaitForSeconds(1f);
        One.SetActive(false);
        oneDone = true;
        Application.Quit();
    }
}

