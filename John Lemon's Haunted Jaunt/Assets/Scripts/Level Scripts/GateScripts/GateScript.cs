using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GateScript : MonoBehaviour
{
    public GameObject[] gateList;
    public GameObject[] lightList;
    public GameObject[] buttonList;
    public GameObject Gun;


    public IEnumerator openGateOne()
    {
        // original -46.7, 2.7, -13.2
        // new Vector3(-46.7,7.2,-13.2) 
        yield return new WaitForSeconds(.5f);
        gateList[0].transform.DOMove(new Vector3(-46f, 7.2f, -13.45f), 3);
        lightList[0].SetActive(true);
        gateList[0].GetComponents<AudioSource>()[0].Play();
    }

    public void closeGateOne()
    {
        gateList[0].transform.DOMove(new Vector3(-46f, 2.7f, -13.45f), 1);
        gateList[0].GetComponents<AudioSource>()[1].Play();
    }

    public IEnumerator openGateTwo()
    {
        // original Vector3(-35.5f,2.7f,14.9f)
        // new Vector3(-35.5f,7.2f,14.9f)
        buttonList[0].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(.5f);
        gateList[1].transform.DOMove(new Vector3(-73.76f, 7.2f, 14.72f), 3);
        lightList[1].SetActive(true);
        gateList[1].GetComponents<AudioSource>()[0].Play();
    }

    public void closeGateTwo()
    {
        gateList[1].transform.DOMove(new Vector3(-73.76f, 2.7f, 14.72f), 1);
        gateList[1].GetComponents<AudioSource>()[1].Play();
    }

    public IEnumerator openGateThree()
    {
        // original Vector3(-105.6f, 2.7f, 0)
        // new Vector3(-105.6f, 7.2f, 0)
        buttonList[1].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(.5f);
        gateList[2].transform.DOMove(new Vector3(-105.15f, 7.2f, -.11f), 3);
        lightList[2].SetActive(true);
        gateList[2].GetComponents<AudioSource>()[0].Play();
    }

    public void closeGateThree()
    {
        gateList[2].transform.DOMove(new Vector3(-105.15f, 2.7f, -.11f), 1);
        gateList[2].GetComponents<AudioSource>()[1].Play();
    }

    public IEnumerator openGateFour()
    {
        // original Vector3(-146.3f, 2.7f, 0)
        // new Vector3(-146.3f, 7.2f, 0)
        buttonList[2].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(.5f);
        gateList[3].transform.DOMove(new Vector3(-145.69f, 7.2f, -.11f), 3);
        lightList[3].SetActive(true);
        gateList[3].GetComponents<AudioSource>()[0].Play();
    }

    public IEnumerator closeGateFour()
    {
        yield return new WaitForSeconds(3.5f);
        gateList[3].transform.DOMove(new Vector3(-145.69f, 2.7f, -.11f), 1);
        Gun.GetComponent<ShootLaser>().setGateFour(false);
        lightList[3].SetActive(false);
        gateList[3].GetComponents<AudioSource>()[1].Play();
    }

    public IEnumerator openGateFive()
    {
        // original Vector3(-153.5f,2.7f,18.1f)
        // new Vector3(-153.5f,7.2f,18.1f)
        buttonList[3].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(.5f);
        gateList[4].transform.DOMove(new Vector3(-153.61f, 7.2f, 17.42f), 3);
        lightList[4].SetActive(true);
        gateList[4].GetComponents<AudioSource>()[0].Play();
    }

    public void closeGateFive()
    {
        gateList[4].transform.DOMove(new Vector3(-153.61f, 2.7f, 17.42f), 1);
        gateList[4].GetComponents<AudioSource>()[1].Play();
    }

    public IEnumerator openGateSix()
    {
        // original Vector3(-157.4f,2.7f,50.3f)
        // new Vector3(-157.4f,7.2f,50.3f)
        buttonList[4].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(.5f);
        gateList[5].transform.DOMove(new Vector3(-157.46f, 7.2f, 50f), 3);
        lightList[5].SetActive(true);
        gateList[5].GetComponents<AudioSource>()[0].Play();
    }

    public IEnumerator closeGateSix()
    {
        yield return new WaitForSeconds(5f);
        gateList[5].transform.DOMove(new Vector3(-157.46f, 2.7f, 50f), 1);
        Gun.GetComponent<ShootLaser>().setGateSix(false);
        lightList[5].SetActive(false);
        gateList[5].GetComponents<AudioSource>()[1].Play();
    }

    public IEnumerator openGateSeven()
    {
        // original Vector3(-166f,2.7f,64.5f)
        // new Vector3(-166f,7.2f,64.5f)
        buttonList[5].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(.5f);
        gateList[6].transform.DOMove(new Vector3(-165.42f, 7.2f, 64.52f), 3);
        lightList[6].SetActive(true);
        gateList[6].GetComponents<AudioSource>()[0].Play();
    }

    public IEnumerator closeGateSeven()
    {
        yield return new WaitForSeconds(5f);
        gateList[6].transform.DOMove(new Vector3(-165.42f, 2.7f, 64.52f), 1);
        Gun.GetComponent<ShootLaser>().setGateSeven(false);
        lightList[6].SetActive(false);
        gateList[6].GetComponents<AudioSource>()[1].Play();
    }  
}
