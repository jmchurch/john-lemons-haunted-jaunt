using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponActivate : MonoBehaviour
{
    public List<GameObject> playerAssets;

    public GameObject GateController;
    public GameObject Gun;
    public GameObject pedestoolWeapon;

    private bool weaponActive = false;

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !weaponActive)
            {
                Destroy(pedestoolWeapon);
                for (int i = 0; i < playerAssets.Count; i++)
                {
                    playerAssets[i].SetActive(true);
                }
                weaponActive = true;
                Gun.GetComponent<ShootLaser>().setShootingAvailable(true); // changes shooting bool in ShootLaser script
                GetComponent<AudioSource>().Play();
                StartCoroutine(GateController.GetComponent<GateScript>().openGateOne()); // opens gate one
            }
        }
    }
}
