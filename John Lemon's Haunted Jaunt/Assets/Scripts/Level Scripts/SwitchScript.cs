using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class SwitchScript : MonoBehaviour
{

    public GameObject laser;
    public GameObject laserSwitch;
    public GameObject LaserShoot;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKey(KeyCode.E))
        {
            laserSwitch.transform.DOMove(new Vector3(-178.9f, 2.2f, 77.6f), 2);
            laserSwitch.transform.DORotate(new Vector3(0f, 0f, -32f), 2);
            StartCoroutine(waitForSwitch());
            
        }
    }

    IEnumerator waitForSwitch()
    {
        yield return new WaitForSeconds(2);
        laser.SetActive(true);
        StartCoroutine(turnOffLaser());
    }

    IEnumerator turnOffLaser()
    {
        laserSwitch.GetComponents<AudioSource>()[1].Play();
        yield return new WaitForSeconds(7);
        laserSwitch.GetComponents<AudioSource>()[1].Pause();
        laserSwitch.GetComponents<AudioSource>()[0].Play();
        laser.SetActive(false);
        StartCoroutine(restartGame());
    }

    IEnumerator restartGame()
    {
        yield return new WaitForSeconds(10);
        SceneManager.LoadScene("Menu");
    }
}
