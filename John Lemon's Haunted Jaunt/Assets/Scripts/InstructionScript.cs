using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionScript : MonoBehaviour
{
    public GameObject menuPlayer;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            menuPlayer.GetComponent<PlayerMovement>().setCanWalk(true);
            Destroy(this.gameObject);
        }
    }
}
