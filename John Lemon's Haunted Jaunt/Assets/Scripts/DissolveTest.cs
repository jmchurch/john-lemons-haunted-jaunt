using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveTest : MonoBehaviour
{
    public Material dissolveMat;
    public int numMaxLaserPoints = 500; // when changing this, make sure to also change the define in the Dissolve shader

    Camera mainCam;

    List<Vector4> LaserPoints = new List<Vector4>();
    int currLaserIdx = 0;

    void Awake() {
        for (int i = 0; i < numMaxLaserPoints; i++) {
            LaserPoints.Add(Vector4.zero); // init to 0,0,0,0
        }
        dissolveMat.SetVectorArray("_LaserPoints", LaserPoints);
        mainCam = Camera.main;
    }

    void Update()
    {
        //if (Input.GetMouseButton(0)) {
        //    DrawLaser();
        //}
    }

    public void DrawLaser(RaycastHit hit, GameObject obj) {
        Material currentLaserMat = obj.GetComponent<Renderer>().sharedMaterial;

        if (currentLaserMat == dissolveMat) {
            //Debug.DrawRay(mainCam.transform.position, hit.point, Color.red, 2f);
            AddLaserPoint(hit.point);
            dissolveMat.SetVectorArray("_LaserPoints", LaserPoints);
        }
 
    }

    void AddLaserPoint(Vector3 hitPointWS) {
        LaserPoints[currLaserIdx] = hitPointWS;
        currLaserIdx = (currLaserIdx + 1) % LaserPoints.Count;
    }

    // resets material when play mode stops
    void OnDestroy() {
        for (int i = 0; i < LaserPoints.Count; i++) {
            LaserPoints[i] = Vector4.zero;
        }
        dissolveMat.SetVectorArray("_LaserPoints", LaserPoints);
    }

}
