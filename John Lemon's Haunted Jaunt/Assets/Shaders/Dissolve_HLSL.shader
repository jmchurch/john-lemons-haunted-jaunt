Shader "Unlit/Dissolve_HLSL"
{
    // https://www.cyanilux.com/tutorials/urp-shader-code/
    Properties
    {
        [MainTexture] _MainTex ("Texture", 2D) = "white" {}
        _Point ("Point", Vector) = (0,0,0,0)
        _IsPointWS ("Is Point Worldspace? (1 -> true, 0 -> false)", Int) = 0
        //_LaserMaskTex ("Laser Mask Texture", 2D) = "black" {}
        _Amount ("Amount", float) = 0.0
        _EdgeAmount ("Edge Amount", Range(0, 1)) = 0.0
        [HDR] _EdgeColor ("Edge Color", Color) = (0,0,0,0)
    }
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue" = "Transparent"
            "RenderPipeline" = "UniversalRenderPipeline" 
        }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha 

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"            

            struct MeshData
            {
                float4 vertex : POSITION; // OS
                float2 uv : TEXCOORD0;
            };

            struct Interpolators
            {
                float2 uv : TEXCOORD0;
                float4 pos_clip : SV_POSITION; // CS
                float3 positionOS : TEXCOORD1; // OS
                VertexPositionInputs positions : TEXCOORD2; // Contains positionWS (worldspace), positionVS (viewspace), positionCS (clipspace), positionNDC (screenspace)
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            //sampler2D _LaserMaskTex;
            //float4 _LaserMaskTex_ST;

            float3 _Point;
            bool _IsPointWS;
            float _Amount;
            float _EdgeAmount;
            float4 _EdgeColor;

            //  abitrary max value
            #define MAX_LASER_POINTS 500 // when changing this, make sure to also change the variable in the Dissolve C# script
            float3 _LaserPoints[MAX_LASER_POINTS];


            Interpolators vert (MeshData v)
            {
                Interpolators o;
                o.positions = GetVertexPositionInputs(v.vertex.xyz);
                o.positionOS = v.vertex.xyz;
                o.pos_clip = o.positions.positionCS;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float4 frag (Interpolators i) : SV_Target
            {
                float4 col = tex2D(_MainTex, i.uv);
                for (int idx = 0; idx < MAX_LASER_POINTS; idx++) {
                    float3 p = _LaserPoints[idx];
                    if (length(p) > 0.0) {
                        float3 p_transformed = _IsPointWS ? p : mul(UNITY_MATRIX_M, p);
                        float3 diff =  length(i.positions.positionWS - p_transformed);

                        diff = saturate(diff - _Amount);

                        float laser_mask = smoothstep(0, _EdgeAmount, diff);

                        float laser_mask_edge = step(1, laser_mask);
                        float4 edge_col = _EdgeColor * (1-laser_mask_edge);

                        col = lerp(edge_col, col, laser_mask);
                    }
                }

                return col;
            }
            ENDHLSL
        }
    }
}
