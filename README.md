# README
----
## John Lemon's Haunted Jaunt - Modifications
### John Lemon's Puzzle Adventure
### Premise
- In this game John Lemon is trying to escape from an unkown space station. His only tool is a handy laser rifle that he finds at the start of the game.
---
## Before playing
- Please make sure that you start in the menu scene if you are trying to play the game as intended, have fun :)
- Inputs
	- Movement: WASD
	- Shooting: Mouse 1
	- Reload: R
	- Interact: E
---
## Mechanics
- Laser Bounce
	- The laser bounce uses updates the ArrayList of positions on the line renderer and with every hit reflects the original RayCast's direction
- Laser Dissolve
	- This is all thanks to Grayson (not totally sure how it works). Grayson made a shader that takes the world position of a RayCastHit and increases the alpha in that area on the GameObject 
	- Grayson also implemented the ability to update the texture of the GameObject so that there is a trail effect behind the RayCastHit
- Animations
	- Walking, Shooting, Reloading (Made in Blender, .FBX)
- Reloading cont.
	- Part of the reloading mechanic is the addition of a charge meter for the laser rifle 
- Sound Effects
	- Laser Charge, Laser Shooting, button electricity, voicelines, door opening, door closing
- 3D models
	- JohnLemonFPS rig, Laser Rifle, 3D lettering, lights, gate